package org.eit;

import java.util.Objects;

/**
 * Provides access to selection control operations
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class SelectionImpl implements Selection {

    private static final int BUFFER_BEGIN_INDEX = 0;
    private static final String BEGIN_GREATER_END = "beginIndex can't be greater than endIndex";
    private static final String NEGATIVE_INDEX = "Index can't be lower than 0";
    private static final String END_INDEX_TOO_BIG = "EndIndex can't be bigger than the length of the buffer";
    private static final String BEGIN_BEFORE_BUFFER = "BeginIndex can't be before the Buffer Begin Index";
    private final StringBuilder buffer;
    private int beginIndex;
    private int endIndex;

    /**
     * This constructor is called only by the engine. It can in fact pass the buffer.
     *
     * @param buffer     the buffer passed by reference.
     * @param beginIndex the index of the first character designated by the selection.
     * @param endIndex   the index of the first character after the last character designated by the selection.
     * @throws IndexOutOfBoundsException if the beginIndex is out of bounds
     */
    public SelectionImpl(StringBuilder buffer, int beginIndex, int endIndex) {
        Objects.requireNonNull(buffer);

        if (endIndex < beginIndex)
            throw new IndexOutOfBoundsException(BEGIN_GREATER_END);
        if (beginIndex < 0)
            throw new IndexOutOfBoundsException(NEGATIVE_INDEX);
        if (beginIndex < BUFFER_BEGIN_INDEX)
            throw new IndexOutOfBoundsException(BEGIN_BEFORE_BUFFER);
        this.buffer = buffer;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    /**
     * Constructor setting default values to 0 for beginIndex and endIndex
     *
     * @param buffer the buffer
     */
    public SelectionImpl(StringBuilder buffer) {
        this(buffer, 0, 0);
    }

    /**
     * Provides the index of the first character designated
     * by the selection.
     *
     * @return the beginning index.
     */
    @Override
    public int getBeginIndex() {
        return beginIndex;
    }

    /**
     * Changes the value of the beginning index of the selection
     *
     * @param beginIndex the beginning index to be set.
     * @throws IndexOutOfBoundsException if the beginIndex is out of bounds
     */
    @Override
    public void setBeginIndex(int beginIndex) {
        if (endIndex < beginIndex)
            throw new IndexOutOfBoundsException(BEGIN_GREATER_END);
        if (beginIndex < 0)
            throw new IndexOutOfBoundsException(NEGATIVE_INDEX);
        if (beginIndex < BUFFER_BEGIN_INDEX)
            throw new IndexOutOfBoundsException(BEGIN_BEFORE_BUFFER);
        this.beginIndex = beginIndex;
    }

    /**
     * Provides the index of the first character
     * after the last character designated
     * by the selection.
     *
     * @return the end index
     */
    @Override
    public int getEndIndex() {
        return endIndex;
    }

    /**
     * Changes the value of the end index of the selection
     *
     * @param endIndex the ending index to be set.
     * @throws IndexOutOfBoundsException if the endIndex is out of bounds
     */
    @Override
    public void setEndIndex(int endIndex) {
        if (endIndex < beginIndex)
            throw new IndexOutOfBoundsException(BEGIN_GREATER_END);
        if (endIndex > buffer.length())
            throw new IndexOutOfBoundsException(END_INDEX_TOO_BIG);

        this.endIndex = endIndex;
    }

    /**
     * Provides the index of the first character in the buffer
     *
     * @return the buffer's begin index
     */
    @Override
    public int getBufferBeginIndex() {
        return BUFFER_BEGIN_INDEX;
    }

    /**
     * Provides the index of the first "virtual" character
     * after the end of the buffer
     *
     * @return the post end buffer index
     */
    @Override
    public int getBufferEndIndex() {
        return buffer.length();
    }
}
