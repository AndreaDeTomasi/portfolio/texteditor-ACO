package org.eit;

/**
 * Provide the history of the program execution with capability to travel in it.
 *
 * @author andrea/ewen
 * @version 1.0
 */
public interface UndoManager {

    /**
     * Given a MementoEditor, store elements with the info contained inside the memento.
     *
     * @param m the MementoEditor with the information.
     */
    void store(MementoEditor m);

    /**
     * Go back to the previous history state.
     */
    void undo();

    /**
     * Go to the next history state.
     */
    void redo();

    /**
     * Checks if I'm currently at the first stored state.
     *
     * @return true if I'm on the first state, false elsewhere.
     */
    boolean isFirstState();

    /**
     * Checks if currently I'm at the most recent state
     *
     * @return true if I'm on the last state, false elsewhere.
     */
    boolean isLastState();
}
