package org.eit;

import java.util.Objects;

/**
 * Memento of the engine.
 *
 * @author andrea/ewen
 * @version 1.0
 */
public class MementoEditor implements Memento {

    private final StringBuilder bufferContent;
    private final int beginIndex;
    private final int endIndex;

    /**
     * Constructor
     *
     * @param bufferContent the buffer content we want to save.
     * @param beginIndex    the index of the first character designated by the selection.
     * @param endIndex      the index of the first character after the last character designated by the selection.
     */
    public MementoEditor(StringBuilder bufferContent, int beginIndex, int endIndex) {
        Objects.requireNonNull(bufferContent);

        this.bufferContent = bufferContent;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    /**
     * Allows to obtain the content of the buffer saved in this memento.
     *
     * @return the buffer content.
     */
    public StringBuilder getBuffer() {
        return bufferContent;
    }

    /**
     * Allows to obtain the begin index saved in this memento.
     *
     * @return the begin index.
     */
    public int getBeginIndex() {
        return beginIndex;
    }

    /**
     * Allows to obtain the end index saved in this memento.
     *
     * @return the end index.
     */
    public int getEndIndex() {
        return endIndex;
    }
}
