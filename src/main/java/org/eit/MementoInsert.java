package org.eit;

/**
 * Memento to store a text for inserting text.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class MementoInsert implements Memento {
    private final String text;

    /**
     * Constructor for the memento containing a String field.
     *
     * @param text the string to be saved inside the memento.
     */
    public MementoInsert(String text) {
        this.text = text;
    }

    /**
     * Allows to obtain the text saved in this memento.
     *
     * @return the text saved.
     */
    public String getText() {
        return text;
    }
}
