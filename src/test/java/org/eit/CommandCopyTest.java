package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommandCopyTest {
    private Engine engine;
    private Recorder recorder;
    private Invoker invoker;
    private UndoManager undoManager;

    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);
    }

    @Test
    @DisplayName("Copy test")
    void copyTest() {
        engine.insert("hello world");
        engine.getSelection().setBeginIndex(0);
        engine.getSelection().setEndIndex(3);
        invoker.setCommand(new CommandCopy(engine, recorder,undoManager));
        invoker.executeCommand();
        assertEquals("hel", engine.getClipboardContents());
    }

    @Test
    @DisplayName("Memento test")
    void getMementoTest() {
        Command command = new CommandCopy(engine, recorder,undoManager);
        assertEquals(MementoEmpty.class, command.getMemento().getClass());
    }
}
