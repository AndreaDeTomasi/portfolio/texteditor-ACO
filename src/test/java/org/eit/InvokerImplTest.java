package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class InvokerImplTest {
    private InvokerImpl invoker;

    @BeforeEach
    void setUp() {
        invoker = new InvokerImpl();
    }

    @Test
    @DisplayName("Default constructor execute")
    void defaultConstructorExecute() {
        assertThrows(NullPointerException.class, () -> invoker.executeCommand());
    }

    @Test
    @DisplayName("Execute commands")
    void executeCommands() {
        Engine engine = new EngineImpl();
        Recorder recorder = new RecorderImpl();
        UndoManager undoManager = new UndoManagerImpl(engine);
        invoker.setText("hello world");
        Command c1 = new CommandInsert(engine, recorder, invoker,undoManager);
        invoker.setCommand(c1);
        invoker.executeCommand();
        assertEquals("hello world", engine.getBufferContents());
    }

    @Test
    void setGetText() {
        invoker.setText("aer");
        assertEquals("aer", invoker.getText());
    }


    @Test
    void setBeginIndex() {
        invoker.setBeginIndex(5);
        assertEquals(5, invoker.getBeginIndex());
    }


    @Test
    void setEndIndex() {
        invoker.setEndIndex(4);
        assertEquals(4, invoker.getEndIndex());
    }
}
