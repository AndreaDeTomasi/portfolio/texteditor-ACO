package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CommandSetBeginIndexTest {
    private Engine engine;
    private Recorder recorder;
    private Invoker invoker;
    private UndoManager undoManager;


    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);

    }

    @Test
    @DisplayName("Set begin")
    void setBeginTest() {
        Command command = new CommandSetBeginIndex(engine.getSelection(), recorder, invoker,undoManager);
        engine.insert("hello");
        invoker.setBeginIndex(2);
        invoker.setCommand(command);
        invoker.executeCommand();
        assertEquals(2, engine.getSelection().getBeginIndex());
    }

    @Test
    @DisplayName("Get memento")
    void getMementoTest() {
        Command command = new CommandSetBeginIndex(engine.getSelection(), recorder, invoker,undoManager);
        assertEquals(MementoSetBeginIndex.class, command.getMemento().getClass());
    }

    @Test
    @DisplayName("Set memento")
    void setMementoTest() {
        Command command = new CommandSetBeginIndex(engine.getSelection(), recorder, invoker,undoManager);
        Memento memento = new MementoSetBeginIndex(2);
        command.setMemento(memento);
        assertEquals(2, invoker.getBeginIndex());
    }

}