package org.eit;

import java.util.Objects;

/**
 * Provide an executable command named insert with the possibility to reproduce it.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class CommandInsert implements Command {

    private final Engine engine;
    private final Recorder recorder;
    private final Invoker invoker;
    private final UndoManager undoManager;

    /**
     * command to insert a string at the selection position
     *
     * @param engine   the engine to be modified
     * @param recorder the recorder
     * @param invoker  the invoker
     */
    public CommandInsert(Engine engine, Recorder recorder, Invoker invoker, UndoManager undoManager) {
        Objects.requireNonNull(undoManager);

        Objects.requireNonNull(engine);
        Objects.requireNonNull(recorder);
        Objects.requireNonNull(invoker);
        this.engine = engine;
        this.recorder = recorder;
        this.undoManager = undoManager;

        this.invoker = invoker;
    }

    /**
     * Executes this command saving itself in the recorder.
     */
    @Override
    public void execute() {
        recorder.save(this);
        engine.insert(invoker.getText());

        undoManager.store((MementoEditor) engine.getMemento());
    }

    /**
     * Creates a memento containing the information to reproduce something.
     *
     * @return the memento.
     */
    @Override
    public Memento getMemento() {
        return new MementoInsert(invoker.getText());
    }

    /**
     * Given a memento, set the surrounding elements with the info contained inside the memento.
     *
     * @param m the memento with the information.
     */
    @Override
    public void setMemento(Memento m) {
        Objects.requireNonNull(m);
        invoker.setText(((MementoInsert) m).getText());
    }
}
