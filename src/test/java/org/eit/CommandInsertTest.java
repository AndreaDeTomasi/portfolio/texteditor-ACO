package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CommandInsertTest {
    private Engine engine;
    private Recorder recorder;
    private Invoker invoker;
    private UndoManager undoManager;


    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);

    }

    @Test
    @DisplayName("Insert test")
    void insert() {
        Command command = new CommandInsert(engine, recorder, invoker,undoManager);
        invoker.setText("hello world");
        invoker.setCommand(command);
        invoker.executeCommand();
        assertEquals("hello world", engine.getBufferContents());
    }

    @Test
    @DisplayName("Get memento")
    void getMementoTest() {
        Command command = new CommandInsert(engine, recorder, invoker,undoManager);
        assertEquals(MementoInsert.class, command.getMemento().getClass());
    }

    @Test
    @DisplayName("Set memento")
    void setMementoTest() {
        Command command = new CommandInsert(engine, recorder, invoker,undoManager);
        Memento memento = new MementoInsert("hello");
        command.setMemento(memento);
        assertEquals("hello", invoker.getText());
    }
}