package org.eit;

import java.util.Objects;

/**
 * Main API for the text editing engine
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class EngineImpl implements Engine {

    private final Selection selection;
    private StringBuilder buffer;
    private String clipboard;

    /**
     * Constructor of EngineImpl
     *
     * @param buffer    the StringBuilder containing the buffer
     * @param clipboard the clipboard with the string in memory
     * @param selection the selection object
     */
    public EngineImpl(StringBuilder buffer, String clipboard, Selection selection) {
        Objects.requireNonNull(buffer);
        Objects.requireNonNull(clipboard);
        Objects.requireNonNull(selection);
        this.buffer = buffer;
        this.clipboard = clipboard;
        this.selection = selection;
    }

    /**
     * Default constructor. Initializes an empty buffer, an empty clipboard and a selection starting and ending at 0,0.
     */
    public EngineImpl() {
        this.buffer = new StringBuilder();
        this.clipboard = "";
        this.selection = new SelectionImpl(this.buffer, 0, 0);
    }

    /**
     * Provides access to the selection control object
     *
     * @return the selection object
     */
    @Override
    public Selection getSelection() {
        return selection;
    }

    /**
     * Provides the whole contents of the buffer, as a string
     *
     * @return a copy of the buffer's contents
     */
    @Override
    public String getBufferContents() {
        return buffer.toString();
    }

    /**
     * Provides the clipboard contents
     *
     * @return a copy of the clipboard's contents
     */
    @Override
    public String getClipboardContents() {
        return clipboard;
    }

    /**
     * Removes the text within the interval
     * specified by the selection control object,
     * from the buffer.
     */
    @Override
    public void cutSelectedText() {
        copySelectedText();
        buffer.delete(selection.getBeginIndex(), selection.getEndIndex());
    }

    /**
     * Copies the text within the interval
     * specified by the selection control object
     * into the clipboard.
     */
    @Override
    public void copySelectedText() {
        clipboard = buffer.substring(selection.getBeginIndex(), selection.getEndIndex());
    }

    /**
     * Replaces the text within the interval specified by the selection object with
     * the contents of the clipboard.
     */
    @Override
    public void pasteClipboard() {
        buffer.replace(selection.getBeginIndex(), selection.getEndIndex(), clipboard);
        selection.setEndIndex(selection.getBeginIndex() + clipboard.length());
        selection.setBeginIndex(selection.getEndIndex());
    }

    /**
     * Inserts a string in the buffer, which replaces the contents of the selection
     *
     * @param s the text to insert
     */
    @Override
    public void insert(String s) {
        Objects.requireNonNull(s);
        buffer.replace(selection.getBeginIndex(), selection.getEndIndex(), s);
        selection.setEndIndex(selection.getBeginIndex() + s.length());
        selection.setBeginIndex(selection.getEndIndex());
    }

    /**
     * Removes the contents of the selection in the buffer
     */
    @Override
    public void delete() {
        if (selection.getBeginIndex() == selection.getEndIndex()) {
            buffer.delete(selection.getBeginIndex() - 1, selection.getEndIndex());
            selection.setBeginIndex(selection.getBeginIndex() - 1);
        } else {
            buffer.delete(selection.getBeginIndex(), selection.getEndIndex());
        }
        selection.setEndIndex(selection.getBeginIndex());

    }

    /**
     * Creates a memento containing the information to reproduce the EngineImpl.
     *
     * @return the memento.
     */
    @Override
    public Memento getMemento() {
        return new MementoEditor(buffer, selection.getBeginIndex(), selection.getEndIndex());
    }

    /**
     * Given a memento, set the surrounding elements with the info contained inside the memento.
     *
     * @param m the memento with the information.
     */
    @Override
    public void setMemento(Memento m) {
        Objects.requireNonNull(m);
        this.buffer = ((MementoEditor) m).getBuffer();
        this.selection.setBeginIndex(((MementoEditor) m).getBeginIndex());
        this.selection.setEndIndex(((MementoEditor) m).getEndIndex());
    }
}
