package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CommandSetEndIndexTest {
    private Engine engine;
    private Recorder recorder;
    private Invoker invoker;
    private UndoManager undoManager;


    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);

    }

    @Test
    @DisplayName("Set end")
    void setEndTest() {
        Command command = new CommandSetEndIndex(engine.getSelection(), recorder, invoker,undoManager);
        engine.insert("hello");
        engine.getSelection().setBeginIndex(0);
        invoker.setEndIndex(2);
        invoker.setCommand(command);
        invoker.executeCommand();
        assertEquals(2, engine.getSelection().getEndIndex());
    }

    @Test
    @DisplayName("Get memento")
    void getMementoTest() {
        Command command = new CommandSetEndIndex(engine.getSelection(), recorder, invoker,undoManager);
        assertEquals(MementoSetEndIndex.class, command.getMemento().getClass());
    }

    @Test
    @DisplayName("Set memento")
    void setMementoTest() {
        Command command = new CommandSetEndIndex(engine.getSelection(), recorder, invoker,undoManager);
        Memento memento = new MementoSetEndIndex(2);
        command.setMemento(memento);
        assertEquals(2, invoker.getEndIndex());
    }

}