package org.eit;

/**
 * Provide access to Memento.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public interface Originator {
    /**
     * Creates a memento containing the information to reproduce something.
     *
     * @return the memento.
     */
    Memento getMemento();

    /**
     * Given a memento, set the surrounding elements with the info contained inside the memento.
     *
     * @param m the memento with the information.
     */
    void setMemento(Memento m);
}
