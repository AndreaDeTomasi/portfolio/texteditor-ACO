package org.eit;

/**
 * Provide an unique interface merging CommandSmall and Originator
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public interface Command extends CommandSmall, Originator {

}
