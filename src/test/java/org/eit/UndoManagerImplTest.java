package org.eit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UndoManagerImplTest {

    private static final String BUFFER_TEXT = "Test of a string";
    private static final String CLIPBOARD_TEXT = "clip";
    private static final int BEGIN_INDEX = 3;
    private static final int END_INDEX = 10;
    private static final StringBuilder MEMENTO_TEXT = new StringBuilder("Test of a string");
    private static final Selection SELECTION_TEXT = new SelectionImpl(new StringBuilder(BUFFER_TEXT), BEGIN_INDEX, END_INDEX);
    private static final Engine ENGINE_TEXT = new EngineImpl(new StringBuilder(BUFFER_TEXT), CLIPBOARD_TEXT, SELECTION_TEXT);
    private static final MementoEditor MEMENTO_EDITOR = new MementoEditor(MEMENTO_TEXT,BEGIN_INDEX,END_INDEX);
    private static final MementoEditor MEMENTO_EDITOR_TWO = new MementoEditor(MEMENTO_TEXT,2,END_INDEX);
    private UndoManager undoManager;

    @org.junit.jupiter.api.BeforeEach
    void initialize() {
        undoManager = new UndoManagerImpl(ENGINE_TEXT);
    }

    @Test
    @DisplayName("Default Selection constructor")
    void defConstructor() {
        assertTrue(undoManager.isFirstState());
        assertTrue(undoManager.isLastState());
        assertEquals(MEMENTO_TEXT, MEMENTO_EDITOR.getBuffer());
        assertEquals(BEGIN_INDEX,SELECTION_TEXT.getBeginIndex());
        assertEquals(END_INDEX,SELECTION_TEXT.getEndIndex());
    }

    @Test
    @DisplayName("Check store")
    void store() {
        undoManager.store(MEMENTO_EDITOR);
        assertFalse(undoManager.isFirstState());
        assertTrue(undoManager.isLastState());

        SELECTION_TEXT.setBeginIndex(1);
        undoManager.store(MEMENTO_EDITOR);
        assertEquals(1,ENGINE_TEXT.getSelection().getBeginIndex());

        ENGINE_TEXT.setMemento(MEMENTO_EDITOR_TWO);
        undoManager.store(MEMENTO_EDITOR_TWO);
        assertEquals(2,ENGINE_TEXT.getSelection().getBeginIndex());

        ENGINE_TEXT.getSelection().setBeginIndex(3);
    }

    @Test
    @DisplayName("Check undo")
    void undo() {
        undoManager.store(MEMENTO_EDITOR);
        undoManager.undo();
        assertTrue(undoManager.isFirstState());
        assertFalse(undoManager.isLastState());

        ENGINE_TEXT.setMemento(MEMENTO_EDITOR_TWO);
        undoManager.store(MEMENTO_EDITOR_TWO);
        assertEquals(2,ENGINE_TEXT.getSelection().getBeginIndex());
        ENGINE_TEXT.setMemento(MEMENTO_EDITOR);
        undoManager.store(MEMENTO_EDITOR);
        assertEquals(3,ENGINE_TEXT.getSelection().getBeginIndex());
        undoManager.undo();
        assertEquals(2,ENGINE_TEXT.getSelection().getBeginIndex());
    }

    @Test
    @DisplayName("Check redo")
    void redo() {
        undoManager.store(MEMENTO_EDITOR);
        undoManager.undo();
        undoManager.redo();
        assertFalse(undoManager.isFirstState());
        assertTrue(undoManager.isLastState());
        undoManager.redo();
        assertFalse(undoManager.isFirstState());
        assertTrue(undoManager.isLastState());

        ENGINE_TEXT.setMemento(MEMENTO_EDITOR_TWO);
        undoManager.store(MEMENTO_EDITOR_TWO);
        assertEquals(2,ENGINE_TEXT.getSelection().getBeginIndex());
        ENGINE_TEXT.setMemento(MEMENTO_EDITOR);
        undoManager.store(MEMENTO_EDITOR);
        assertEquals(3,ENGINE_TEXT.getSelection().getBeginIndex());
        undoManager.undo();
        assertEquals(2,ENGINE_TEXT.getSelection().getBeginIndex());
        undoManager.redo();
        assertEquals(3,ENGINE_TEXT.getSelection().getBeginIndex());
    }

    @Test
    @DisplayName("Check isFirstState")
    void isFirstState() {
        assertTrue(undoManager.isFirstState());
        undoManager.undo();
        assertTrue(undoManager.isFirstState());
        undoManager.store(MEMENTO_EDITOR);
        assertFalse(undoManager.isFirstState());
        undoManager.undo();
        assertTrue(undoManager.isFirstState());
    }

    @Test
    @DisplayName("Check isLastState")
    void isLastState() {
        assertTrue(undoManager.isLastState());
        undoManager.undo();
        assertTrue(undoManager.isLastState());
        undoManager.store(MEMENTO_EDITOR);
        assertTrue(undoManager.isLastState());
        undoManager.undo();
        assertFalse(undoManager.isLastState());
    }
}
