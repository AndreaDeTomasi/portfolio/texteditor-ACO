package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CommandCutTest {
    private Engine engine;
    private Recorder recorder;
    private Invoker invoker;
    private UndoManager undoManager;


    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);

    }

    @Test
    @DisplayName("Cut command test")
    void cutTest() {
        engine.insert("Hello world");
        engine.getSelection().setBeginIndex(1);
        engine.getSelection().setEndIndex(4);
        invoker.setCommand(new CommandCut(engine, recorder,undoManager));
        invoker.executeCommand();
        String result = engine.getClipboardContents();
        assertEquals("ell", result);
        String remaining = engine.getBufferContents();
        assertEquals("Ho world", remaining);
    }

    @Test
    @DisplayName("Memento test")
    void getMementoTest() {
        Command command = new CommandCut(engine, recorder,undoManager);
        assertEquals(MementoEmpty.class, command.getMemento().getClass());
    }
}
