package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MementoSetBeginIndexTest {
    private Memento memento;

    @BeforeEach
    void setUp() {
        memento = new MementoSetBeginIndex(4);
    }

    @Test
    void getBeginIndex() {
        assertEquals(4, ((MementoSetBeginIndex) memento).getBeginIndex());
    }
}