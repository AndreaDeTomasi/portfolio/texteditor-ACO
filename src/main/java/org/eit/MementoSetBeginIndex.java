package org.eit;

/**
 * Memento to store a beginIndex.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class MementoSetBeginIndex implements Memento {
    private final int beginIndex;

    /**
     * Memento constructor to save an int as begin index.
     *
     * @param beginIndex the begin index.
     */
    public MementoSetBeginIndex(int beginIndex) {
        this.beginIndex = beginIndex;
    }

    /**
     * Allows to obtain the begin index saved in this memento.
     *
     * @return the begin index saved.
     */
    public int getBeginIndex() {
        return beginIndex;
    }
}
