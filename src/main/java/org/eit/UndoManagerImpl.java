package org.eit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Provide the history of the program execution with capability to travel in it.
 *
 * @author andrea/ewen
 * @version 1.0
 */
public class UndoManagerImpl implements UndoManager {

    /**
     * Contains the past states and the current state.
     */
    private final List<MementoEditor> pastState;

    /**
     * Contains the future state, if any.
     */
    private final List<MementoEditor> futureState;

    private final Engine engine;

    /**
     * Constructor
     *
     * @param engine the Engine object.
     */
    public UndoManagerImpl(Engine engine) {
        Objects.requireNonNull(engine);
        pastState = new ArrayList<>();
        futureState = new ArrayList<>();
        this.engine = engine;
        pastState.add((MementoEditor) engine.getMemento());
    }

    /**
     * Given a MementoEditor, store elements with the info contained inside the memento.
     * Clears the future states previously saved.
     *
     * @param m the MementoEditor with the information.
     */
    public void store(MementoEditor m) {
        Objects.requireNonNull(m);
        futureState.clear();
        pastState.add(m);
    }

    /**
     * Go back to the previous history state, if there is any.
     */
    public void undo() {
        if (pastState.size() > 1) {
            futureState.add(pastState.get(pastState.size() - 1));
            pastState.remove(pastState.size() - 1);
            engine.setMemento(pastState.get(pastState.size() - 1));
        }
    }

    /**
     * Go to the next history state, if there is any.
     */
    public void redo() {
        if (!futureState.isEmpty()) {
            pastState.add(futureState.get(futureState.size() - 1));
            engine.setMemento(futureState.get(futureState.size() - 1));
            futureState.remove(futureState.size() - 1);
        }
    }

    /**
     * Checks if I'm currently at the first stored state.
     *
     * @return true if I'm on the first state, false elsewhere.
     */
    public boolean isFirstState() {
        return pastState.size() == 1;
    }

    /**
     * Checks if currently I'm at the most recent state
     *
     * @return true if I'm on the last state, false elsewhere.
     */
    public boolean isLastState() {
        return futureState.isEmpty();
    }

    /**
     * Gets the current pointed engine
     *
     * @return the engine currently used.
     */
    public Engine getEngine() {
        return this.engine;
    }
}
