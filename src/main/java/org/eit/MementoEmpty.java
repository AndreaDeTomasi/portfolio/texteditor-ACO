package org.eit;

/**
 * Provides an empty memento. Can be used by classes without parameters to save.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class MementoEmpty implements Memento {
}
