package org.eit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EngineTest {
    private static final String BUFFER_TEXT = "Test of a string";
    private static final String CLIPBOARD_TEXT = "clip";
    private static final int BEGIN_INDEX = 3;
    private static final int END_INDEX = 10;
    private static final Selection SELECTION_TEXT = new SelectionImpl(new StringBuilder(BUFFER_TEXT), BEGIN_INDEX, END_INDEX);
    private Engine engine;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        engine = new EngineImpl();
    }

    void setUpNotNull() {
        engine = new EngineImpl(new StringBuilder(BUFFER_TEXT), CLIPBOARD_TEXT, SELECTION_TEXT);
    }

    @Test
    @DisplayName("Check not null constructor")
    void EngineImpl() {
        setUpNotNull();
        Selection selection = engine.getSelection();
        assertEquals(3, selection.getBeginIndex());
        assertEquals(10, selection.getEndIndex());
        assertEquals("Test of a string", engine.getBufferContents());
    }

    @Test
    @DisplayName("Buffer must be empty after initialisation")
    void getSelection() {
        Selection selection = engine.getSelection();
        assertEquals(selection.getBufferBeginIndex(), selection.getBeginIndex());
        assertEquals("", engine.getBufferContents());
    }

    @Test
    @DisplayName("Check getBufferContents")
    void getBufferContents() {
        assertEquals("", engine.getBufferContents());
        engine.insert("inserted");
        assertEquals("inserted", engine.getBufferContents());
    }

    @Test
    @DisplayName("Check getClipboardContents")
    void getClipboardContents() {
        assertEquals("", engine.getClipboardContents());
        engine.insert("inserted");
        Selection selection = engine.getSelection();
        selection.setBeginIndex(2);
        selection.setEndIndex(4);
        engine.copySelectedText();
        assertEquals("se", engine.getClipboardContents());
    }

    @Test
    @DisplayName("Check cutSelectedText")
    void cutSelectedText() {
        engine.insert("inserted");
        Selection selection = engine.getSelection();
        selection.setBeginIndex(2);
        selection.setEndIndex(4);
        engine.cutSelectedText();
        assertEquals("se", engine.getClipboardContents());
        assertEquals("inrted", engine.getBufferContents());
    }

    @Test
    @DisplayName("Check copySelectedText")
    void copySelectedText() {
        engine.insert("inserted");
        Selection selection = engine.getSelection();
        selection.setBeginIndex(2);
        selection.setEndIndex(4);
        engine.copySelectedText();
        assertEquals("se", engine.getClipboardContents());
        assertEquals("inserted", engine.getBufferContents());
    }

    @Test
    @DisplayName("Check pasteClipboard")
    void pasteClipboard() {
        engine.insert("inserted");
        Selection selection = engine.getSelection();
        selection.setBeginIndex(2);
        selection.setEndIndex(4);
        engine.copySelectedText();
        selection.setBeginIndex(0);
        selection.setEndIndex(0);
        engine.pasteClipboard();
        assertEquals("seinserted", engine.getBufferContents());
        assertEquals(2, selection.getBeginIndex());
        assertEquals(2, selection.getEndIndex());
    }

    @Test
    @DisplayName("Check insert")
    void insert() {
        assertEquals("", engine.getBufferContents());
        engine.insert("s");
        assertEquals("s", engine.getBufferContents());
        assertEquals(1, engine.getSelection().getEndIndex());
        assertEquals(1, engine.getSelection().getBeginIndex());
    }

    @Test
    @DisplayName("Check delete")
    void delete() {
        engine.insert("inserted");
        Selection selection = engine.getSelection();
        selection.setBeginIndex(2);
        selection.setEndIndex(4);
        engine.delete();
        assertEquals("inrted", engine.getBufferContents());
        assertEquals(2, engine.getSelection().getEndIndex());
        assertEquals(2, engine.getSelection().getBeginIndex());
    }

    @Test
    @DisplayName("Check delete no selection")
    void delete2() {
        engine.insert("inserted");
        engine.delete();
        assertEquals("inserte", engine.getBufferContents());
        assertEquals(7, engine.getSelection().getEndIndex());
        assertEquals(7, engine.getSelection().getBeginIndex());
    }
}
