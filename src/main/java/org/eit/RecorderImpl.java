package org.eit;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Provides access to saving operations with macro.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class RecorderImpl implements Recorder {
    private final List<Command> commands;
    private final List<Memento> mementos;
    private boolean started = false;

    /**
     * Default constructor of the Recorder.
     */
    public RecorderImpl() {
        commands = new ArrayList<>();
        mementos = new ArrayList<>();
    }

    /**
     * Save the current value of the text edited.
     *
     * @param command the command that has to be saved
     * @throws InvalidParameterException if the parameter is null.
     */
    @Override
    public void save(Command command) {
        Objects.requireNonNull(command);
        if (started) {
            commands.add(command);
            mementos.add(command.getMemento());
        }
    }

    /**
     * Starts recording the macro. Empties it first.
     */
    @Override
    public void start() {
        started = true;
        commands.clear();
    }

    /**
     * Stops recording the macro.
     */
    @Override
    public void stop() {
        started = false;
    }

    /**
     * Replays the saved macro. Before replaying stops the recording otherwise it will never stop replaying.
     */
    @Override
    public void replay() {
        this.stop();
        Command c;
        for (int i = 0; i < commands.size(); i++) {
            c = commands.get(i);
            c.setMemento(mementos.get(i));
            c.execute();
        }
    }

    /**
     * Checks if the recording has been started.
     *
     * @return the status of the recording.
     */
    public boolean isStarted() {
        return started;
    }
}
