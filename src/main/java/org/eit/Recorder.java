package org.eit;

import java.security.InvalidParameterException;

/**
 * Provides access to saving operations with macro.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public interface Recorder {

    /**
     * Save the current value of the text edited.
     *
     * @param command the command that has to be saved
     * @throws InvalidParameterException if the parameter is null.
     */
    void save(Command command) throws InvalidParameterException;

    /**
     * Starts recording the macro. Empties it first.
     */
    void start();

    /**
     * Stops recording the macro.
     */
    void stop();

    /**
     * Replays the saved macro.
     */
    void replay();
}
