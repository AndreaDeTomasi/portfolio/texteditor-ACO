package org.eit;

/**
 * Provide a set of commands ready to be executed
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public interface CommandSmall {
    /**
     * Executes this command.
     */
    void execute();
}
