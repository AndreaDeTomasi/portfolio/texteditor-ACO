package org.eit;

import java.util.Objects;

/**
 * Invoker used to execute Commands.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class InvokerImpl implements Invoker {
    private Command command;
    private String text;
    private int beginIndex;
    private int endIndex;


    /**
     * Sets the command.
     *
     * @param c the command to be set.
     */
    @Override
    public void setCommand(Command c) {
        Objects.requireNonNull(c);
        this.command = c;
    }

    /**
     * Executes the command saved.
     *
     * @throws NullPointerException if no command is saved.
     */
    @Override
    public void executeCommand() throws NullPointerException {
        if (command == null)
            throw new NullPointerException("No command stored.");
        command.execute();
    }

    /**
     * Obtain the saved string.
     *
     * @return the saved string.
     */
    @Override
    public String getText() {
        return text;
    }

    /**
     * Allows to save a string that will be used by a command.
     *
     * @param text the string to be saved.
     */
    @Override
    public void setText(String text) {
        Objects.requireNonNull(text);
        this.text = text;
    }

    /**
     * Obtain the saved begin index.
     *
     * @return the saved begin index.
     */
    @Override
    public int getBeginIndex() {
        return beginIndex;
    }

    /**
     * Allows to save a begin index that will be used by a command.
     *
     * @param beginIndex the index to be saved.
     */
    @Override
    public void setBeginIndex(int beginIndex) {

        this.beginIndex = beginIndex;
    }

    /**
     * Obtain the saved end index.
     *
     * @return the saved end index.
     */
    @Override
    public int getEndIndex() {
        return endIndex;
    }

    /**
     * Allows to save an end index that will be used by a command.
     *
     * @param endIndex the index to be saved.
     */
    @Override
    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }
}
