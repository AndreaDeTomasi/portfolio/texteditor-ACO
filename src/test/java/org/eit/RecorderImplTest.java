package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.*;

class RecorderImplTest {
    private Recorder recorder;
    private Engine engine;
    private Invoker invoker;
    private UndoManager undoManager;

    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);
    }

    @Test
    @DisplayName("Save a null command")
    void saveNull() {
        recorder = new RecorderImpl();
        assertThrows(NullPointerException.class, () -> recorder.save(null));
    }

    @Test
    @DisplayName("Complete run test")
    void completeTest() {
        Command cutCommand = new CommandCut(engine, recorder,undoManager);
        recorder.start();
        engine.insert("Hello world");
        engine.getSelection().setBeginIndex(2);
        engine.getSelection().setEndIndex(4);
        invoker.setCommand(cutCommand);
        invoker.executeCommand();
        recorder.stop();
        recorder.replay();
        assertEquals("Heworld", engine.getBufferContents());
    }

    @Test
    @DisplayName("Complete run with clear")
    void completeClearTest() {
        engine.insert("Hello world");
        engine.getSelection().setBeginIndex(2);
        engine.getSelection().setEndIndex(4);
        invoker.setCommand(new CommandCut(engine, recorder,undoManager));
        invoker.executeCommand();
        // Heo world
        recorder.start();
        engine.getSelection().setBeginIndex(3);
        engine.getSelection().setEndIndex(3);
        invoker.setText("sun");
        invoker.setCommand(new CommandInsert(engine, recorder, invoker,undoManager));
        invoker.executeCommand();
        recorder.stop();
        recorder.replay();
        assertEquals("Heosunsun world", engine.getBufferContents());
    }

    @Test
    @DisplayName("check isStarted")
    void isStartedTest() {
        assertFalse(((RecorderImpl) recorder).isStarted());
        recorder.start();
        assertTrue(((RecorderImpl) recorder).isStarted());
    }

}