package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MementoSetEndIndexTest {
    private Memento memento;

    @BeforeEach
    void setUp() {
        memento = new MementoSetEndIndex(3);
    }

    @Test
    void getEndIndex() {
        assertEquals(3, ((MementoSetEndIndex) memento).getEndIndex());
    }
}