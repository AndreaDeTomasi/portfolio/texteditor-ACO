package org.eit;

import java.util.Objects;

/**
 * Provide an executable command named setBeginIndex with the possibility to reproduce it.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class CommandSetBeginIndex implements Command {

    private final Selection selection;
    private final Recorder recorder;
    private final Invoker invoker;
    private final UndoManager undoManager;

    /**
     * Command to set the BeginIndex for the selection.
     *
     * @param selection the selection.
     * @param recorder  the recorder.
     * @param invoker   the invoker.
     */
    public CommandSetBeginIndex(Selection selection, Recorder recorder, Invoker invoker, UndoManager undoManager) {
        Objects.requireNonNull(undoManager);

        Objects.requireNonNull(selection);
        Objects.requireNonNull(recorder);
        Objects.requireNonNull(invoker);
        this.selection = selection;
        this.recorder = recorder;
        this.undoManager = undoManager;

        this.invoker = invoker;
    }

    /**
     * Executes this command saving itself in the recorder.
     */
    @Override
    public void execute() {
        recorder.save(this);
        selection.setBeginIndex(invoker.getBeginIndex());

        undoManager.store((MementoEditor) (((UndoManagerImpl) undoManager).getEngine()).getMemento());
    }

    /**
     * Creates a memento containing the information to reproduce something.
     *
     * @return the memento.
     */
    @Override
    public Memento getMemento() {
        return new MementoSetBeginIndex(invoker.getBeginIndex());
    }

    /**
     * Given a memento, set the surrounding elements with the info contained inside the memento.
     *
     * @param m the memento with the information.
     */
    @Override
    public void setMemento(Memento m) {
        Objects.requireNonNull(m);
        invoker.setBeginIndex(((MementoSetBeginIndex) m).getBeginIndex());
    }
}
