package org.eit;

/**
 * Invoker used to execute Commands.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public interface Invoker {
    /**
     * Sets the command.
     *
     * @param c the command to be set.
     */
    void setCommand(Command c);

    /**
     * Executes the command saved.
     *
     * @throws NullPointerException if no command is saved.
     */
    void executeCommand() throws NullPointerException;

    /**
     * Obtain the saved string.
     *
     * @return the saved string.
     */
    String getText();

    /**
     * Allows to save a string that will be used by a command.
     *
     * @param text the string to be saved.
     */
    void setText(String text);

    /**
     * Obtain the saved begin index.
     *
     * @return the saved begin index.
     */
    int getBeginIndex();

    /**
     * Allows to save a begin index that will be used by a command.
     *
     * @param beginIndex the index to be saved.
     */
    void setBeginIndex(int beginIndex);

    /**
     * Obtain the saved end index.
     *
     * @return the saved end index.
     */
    int getEndIndex();

    /**
     * Allows to save an end index that will be used by a command.
     *
     * @param endIndex the index to be saved.
     */
    void setEndIndex(int endIndex);
}
