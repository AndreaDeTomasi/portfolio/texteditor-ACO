package org.eit;

import java.util.Objects;

/**
 * Provide an executable command named cut with the possibility to reproduce it.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class CommandCut implements Command {

    private final Engine engine;
    private final Recorder recorder;
    private final UndoManager undoManager;

    /**
     * Command to execute a cut of the selection.
     *
     * @param engine   the engine to modify.
     * @param recorder the recorder.
     */
    public CommandCut(Engine engine, Recorder recorder, UndoManager undoManager) {
        Objects.requireNonNull(undoManager);

        Objects.requireNonNull(engine);
        Objects.requireNonNull(recorder);
        this.engine = engine;
        this.undoManager = undoManager;

        this.recorder = recorder;
    }

    /**
     * Executes this command saving itself in the recorder.
     */
    @Override
    public void execute() {
        recorder.save(this);
        engine.cutSelectedText();

        undoManager.store((MementoEditor) engine.getMemento());
    }

    /**
     * Creates a memento containing the information to reproduce something.
     *
     * @return the memento.
     */
    @Override
    public Memento getMemento() {
        return new MementoEmpty();
    }

    /**
     * Given a memento, set the surrounding elements with the info contained inside the memento.
     *
     * @param m the memento with the information.
     */
    @Override
    public void setMemento(Memento m) {
        // This method is empty because this command has no parameter to set.
    }
}
