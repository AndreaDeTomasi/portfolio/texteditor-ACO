package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommandDeleteTest {
    private Engine engine;
    private Recorder recorder;
    private Invoker invoker;
    private UndoManager undoManager;


    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);

    }

    @Test
    @DisplayName("Delete command test")
    void deleteTest() {
        engine.insert("Hello world");
        invoker.setCommand(new CommandDelete(engine, recorder,undoManager));
        engine.getSelection().setBeginIndex(4);
        engine.getSelection().setEndIndex(6);
        invoker.executeCommand();
        String result = engine.getBufferContents();
        String clipboard = engine.getClipboardContents();
        assertEquals("Hellworld", result);
        assertEquals("", clipboard);
    }

    @Test
    @DisplayName("Memento test")
    void getMementoTest() {
        Command command = new CommandDelete(engine, recorder,undoManager);
        assertEquals(MementoEmpty.class, command.getMemento().getClass());
    }
}
