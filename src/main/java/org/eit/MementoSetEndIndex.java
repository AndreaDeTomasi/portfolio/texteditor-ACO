package org.eit;

/**
 * Memento to store an endIndex.
 *
 * @author Andrea/Ewen
 * @version 1.0
 */
public class MementoSetEndIndex implements Memento {
    private final int endIndex;

    /**
     * Memento constructor to save an int as end index.
     *
     * @param endIndex the end index to be saved.
     */
    public MementoSetEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    /**
     * Allows to obtain the end index saved in this memento.
     *
     * @return the end index saved.
     */
    public int getEndIndex() {
        return endIndex;
    }
}
