package org.eit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SelectionTest {
    private static final String SELECTION_TEXT = "Test of a string";
    private static final int BEGIN_INDEX = 3;
    private static final int END_INDEX = 10;
    private Selection selection;

    void initialize() {
        selection = new SelectionImpl(new StringBuilder(SELECTION_TEXT), BEGIN_INDEX, END_INDEX);
    }

    StringBuilder defaultBuilder() {
        return new StringBuilder(SELECTION_TEXT);
    }

    @Test
    @DisplayName("Default Selection constructor")
    void defConstructor() {
        Selection selection = new SelectionImpl(defaultBuilder());
        assertEquals(0, selection.getBeginIndex());
        assertEquals(0, selection.getEndIndex());
    }


    @Test
    @DisplayName("Selection empty after initialization")
    void emptyInitialization() {
        Selection selection = new SelectionImpl(new StringBuilder(), 0, 0);
        assertEquals(selection.getBufferBeginIndex(), selection.getBeginIndex());
    }


    @Test
    @DisplayName("Selection can't be initialized with initialIndex > endIndex")
    void initialMajEnd() {
        StringBuilder builder = defaultBuilder();
        assertThrows(IndexOutOfBoundsException.class, () -> selection = new SelectionImpl(builder, 10, 5));
    }

    @Test
    @DisplayName("Selection initialized with indexes less than zero")
    void initialLessZero() {
        StringBuilder builder = defaultBuilder();
        assertThrows(IndexOutOfBoundsException.class, () -> selection = new SelectionImpl(builder, -4, 8));
        assertThrows(IndexOutOfBoundsException.class, () -> selection = new SelectionImpl(builder, 5, -7));
        assertThrows(IndexOutOfBoundsException.class, () -> selection = new SelectionImpl(builder, -3, -10));
    }

    @Test
    @DisplayName("Check getBeginIndex")
    void checkGetBeginIndex() {
        initialize();
        assertEquals(BEGIN_INDEX, selection.getBeginIndex());
    }

    @Test
    @DisplayName("Check getEndIndex")
    void checkGetEndIndex() {
        initialize();
        assertEquals(END_INDEX, selection.getEndIndex());
    }

    @Test
    @DisplayName("Check setBeginIndex")
    void checkSetBeginIndex() {
        initialize();
        selection.setBeginIndex(END_INDEX - 1);
        assertEquals(END_INDEX - 1, selection.getBeginIndex());
    }

    @Test
    @DisplayName("Check setEndIndex")
    void checkSetEndIndex() {
        initialize();
        selection.setEndIndex(BEGIN_INDEX + 1);
        assertEquals(BEGIN_INDEX + 1, selection.getEndIndex());
    }

    @Test
    @DisplayName("Check setBeginIndex with wrong index")
    void checkSetWrongBeginIndex() {
        initialize();
        assertThrows(IndexOutOfBoundsException.class, () -> selection.setBeginIndex(END_INDEX + 1));
    }

    @Test
    @DisplayName("Check setBeginIndex with negative index")
    void checkSetNegativeBeginIndex() {
        initialize();
        assertThrows(IndexOutOfBoundsException.class, () -> selection.setBeginIndex(-1));
    }

    @Test
    @DisplayName("Check setEndIndex with wrong index")
    void checkSetWrongEndIndex() {
        initialize();
        assertThrows(IndexOutOfBoundsException.class, () -> selection.setEndIndex(BEGIN_INDEX - 1));
        int length = SELECTION_TEXT.length() * 10;
        assertThrows(IndexOutOfBoundsException.class, () -> selection.setEndIndex(length));
    }

    @Test
    @DisplayName("Check setEndIndex with negative index")
    void checkSetNegativeEndIndex() {
        initialize();
        assertThrows(IndexOutOfBoundsException.class, () -> selection.setEndIndex(-5));
    }

    @Test
    @DisplayName("Get buffer begin index")
    void checkGetBufferBeginIndex() {
        initialize();
        assertEquals(0, selection.getBufferBeginIndex());
    }

    @Test
    @DisplayName("Get buffer end index")
    void checkGetBufferEndIndex() {
        initialize();
        assertEquals(SELECTION_TEXT.length(), selection.getBufferEndIndex());
    }
}
