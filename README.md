# TextEditor

This is the repository containing the Maven project for the TextEditor ACO project.

[![pipeline status](https://gitlab.istic.univ-rennes1.fr/elaudet/texteditor/badges/main/pipeline.svg)](https://gitlab.istic.univ-rennes1.fr/elaudet/texteditor/-/commits/main)
