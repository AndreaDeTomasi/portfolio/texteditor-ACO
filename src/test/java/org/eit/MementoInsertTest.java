package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MementoInsertTest {
    private Memento memento;

    @BeforeEach
    void setUp() {
        memento = new MementoInsert("hello");
    }

    @Test
    void getText() {
        assertEquals("hello", ((MementoInsert) memento).getText());
    }
}