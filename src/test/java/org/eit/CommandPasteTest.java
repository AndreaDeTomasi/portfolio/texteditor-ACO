package org.eit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommandPasteTest {
    private Engine engine;
    private Recorder recorder;
    private Invoker invoker;
    private UndoManager undoManager;


    @BeforeEach
    void setUp() {
        engine = new EngineImpl();
        recorder = new RecorderImpl();
        invoker = new InvokerImpl();
        undoManager = new UndoManagerImpl(engine);

    }

    @Test
    @DisplayName("Paste command test")
    void pasteTest() {
        engine.insert("hello world");
        engine.getSelection().setBeginIndex(1);
        engine.getSelection().setEndIndex(6);
        engine.copySelectedText();
        engine.getSelection().setEndIndex(8);
        engine.getSelection().setBeginIndex(7);
        invoker.setCommand(new CommandPaste(engine, recorder,undoManager));
        invoker.executeCommand();
        assertEquals("hello wello rld", engine.getBufferContents());
    }

    @Test
    @DisplayName("Memento test")
    void getMementoTest() {
        Command command = new CommandPaste(engine, recorder,undoManager);
        assertEquals(MementoEmpty.class, command.getMemento().getClass());
    }
}
